﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour{
	private static SoundManager instance = null;
	public AudioSource mAudioBGM;
	public AudioSource mAudioCorrect;
	public AudioSource mAudioIncorrect;
	public AudioSource mAudioEnding;
	public static SoundManager Instance {
		get { return instance;}
	}
	void Awake() {
		if (instance != null && instance != this) {
			Destroy (this.gameObject);
		} else {
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);
	}
	void Start()
	{
		SetVolume (1);
		mAudioCorrect.Stop ();
		mAudioIncorrect.Stop ();
		mAudioEnding.Stop ();
		mAudioCorrect.volume = 1; 
		mAudioIncorrect.volume = 1;
		mAudioEnding.volume = 1;
	}
	public void PlayBGM()
	{
		mAudioBGM.Play ();
	}
	public void PlayCorrect()
	{
		mAudioCorrect.Play ();
	}
	public void PlayIncorrect()
	{
		mAudioIncorrect.Play ();
	}
	public void PlayEnding()
	{
		mAudioEnding.Play ();
	}
	public void SetVolume(float volume)
	{
		if (mAudioBGM == null) {
			return;
		}
		mAudioBGM.volume = volume;
	}
}
