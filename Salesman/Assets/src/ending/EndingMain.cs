﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndingMain : MonoBehaviour {
	public Text Count;
	void Start () {
		Count.text = GameSetting.GetStudyScore ();
		if (SoundManager.Instance != null)
						SoundManager.Instance.SetVolume (0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
