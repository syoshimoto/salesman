﻿using UnityEngine;
using System.Collections;

public class KenchoAnswer : MonoBehaviour {
	public static bool  IsCorrectKenchoshozaichi(Ken ken, string syozaichi) {
		return syozaichi == GetSyozaichi(ken) || syozaichi == GetKanjiSyozaichi(ken);
	}
	public static string GetSyozaichi(Ken ken) {
		switch (ken) {
		case Ken.Chiba:
			return "ちば";
		case Ken.Tokyo:
			return "しんじゅく";
		case Ken.Kanagawa:
			return "よこはま";
		case Ken.Ibaraki:
			return "みと";
		case Ken.Saitama:
			return "さいたま";
		case Ken.Gunma:
			return "まえばし";
		case Ken.Tochigi:
			return "うつのみや";
		default:
			return "";
		}
	}
	public static string GetKanjiSyozaichi(Ken ken) {
		switch (ken) {
		case Ken.Chiba:
			return "千葉";
		case Ken.Tokyo:
			return "新宿";
		case Ken.Kanagawa:
			return "横浜";
		case Ken.Ibaraki:
			return "水戸";
		case Ken.Saitama:
			return "さいたま";
		case Ken.Gunma:
			return "前橋";
		case Ken.Tochigi:
			return "宇都宮";
		default:
			return "";
		}
	}
}
