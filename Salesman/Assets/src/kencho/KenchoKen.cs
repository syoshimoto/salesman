﻿using UnityEngine;
using System.Collections;

public class KenchoKen : MonoBehaviour {
	public GameObject Kencho;
    float y = 0;
    float mTime = 0f;
    Vector3 beforePosition;
    Vector3 toPosition;
	SpriteRenderer mSpriteRenderer;
	static Color SelectedColor;
	void Start () {
        y = transform.position.y;
        beforePosition = Camera.main.transform.position;
		mSpriteRenderer = GetComponent<SpriteRenderer> ();
//		SelectedColor = Color.blue + new Color(0, 20, 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (mTime > 0f) {
            Vector3 pos = transform.position;
            Camera.main.transform.position = beforePosition * mTime + toPosition * (1 - mTime);
            mTime -= 0.1f;
        }
	}
	public void SetStateAnswered()
	{
		Kencho.SetActive(true);
		mSpriteRenderer.color = new Color(0.8f, 0.7f, 1f, 0.9f);
	}
    public void AnimationSelectKen()
    {
        float scale = 1.3f;
        transform.localScale = new Vector3 (scale, scale, scale);
        toPosition = transform.position + new Vector3(0, - 10, 0);
        mTime = 1f;
    }
}
