﻿using UnityEngine;
using System.Collections;

public class KenchoKencho : MonoBehaviour {
	int mAnimationTime;
	const int ANIMATION_TIME = 20;
	bool mIsBig = false;
	float mScaleDefault;
	float mScaleBig = 0.6f;
	void Start () {
		mAnimationTime = ANIMATION_TIME;
		mScaleDefault = transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (mIsBig)
			return;
		mAnimationTime --;
		if (mAnimationTime < 0) {
			mAnimationTime = 0;
			mIsBig = true;
			ChangeScale();
		}
	}
	void ChangeScale()
	{
		if (mIsBig)
			transform.localScale = new Vector3 (mScaleBig, mScaleBig, mScaleBig);
		else
			transform.localScale = new Vector3 (mScaleDefault, mScaleDefault, mScaleDefault);
	}
}
