﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class QuizMain : MonoBehaviour {
	public Text mQuestion;
	public GameObject AnswerMaru;
	public GameObject AnswerBatsu;
	public GameObject Explanation;
	public GameObject Before;
	public Text ExplanationText;
	public GameObject MaruBatsu;
	public GameObject btnMaru;
	public GameObject btnBatsu;
	public GameObject btnNext;
	public Text Ken;
	int mCorrectCount = 0;
	public Text CorrectCount;
	enum QuizMainState {
		LookQuestion,
		LookResult,
	}
	QuizMainState mState = QuizMainState.LookQuestion;
	void Start () {
		Init();
	}
	
	// Update is called once per frame
	void Update () {
		switch(mState) {
		case QuizMainState.LookQuestion:
			if (Input.GetKeyDown(KeyCode.Alpha1)) {
				OnPressAnswer(true);
			} else if (Input.GetKeyDown(KeyCode.Alpha2)) {
				OnPressAnswer(false);
			}
			break;
		case QuizMainState.LookResult:
			if (Input.GetKeyDown(KeyCode.Alpha1)) {
				ChangeNext();
			}
			break;
		}
	}
	public void OnPressAnswer(bool IsMaru) {
		if (btnNext.activeSelf) {
			return;
		}
		bool isCollect = QuizAnswer.IsCollect(IsMaru);
		if (IsMaru) {
				MaruBatsu.transform.position = btnMaru.transform.position;
		} else {
				MaruBatsu.transform.position = btnBatsu.transform.position;
		}

		if (isCollect) {
			AnswerMaru.SetActive(true);
			SoundManager.Instance.PlayCorrect();
			mCorrectCount++;
			GameSetting.AddStudy(QuizType.KenDetail);
			CorrectCount.text = mCorrectCount.ToString();
		} else {
			AnswerBatsu.SetActive(true);
			SoundManager.Instance.PlayIncorrect();
		}
		Explanation.gameObject.SetActive(true);
		ExplanationText.text = QuizExplanation.get();
		btnNext.SetActive(true);
		Before.SetActive(false);
		mState = QuizMainState.LookResult;
		GameSetting.AddCount ();
	}
	public void ChangeNext() {
		if (GameSetting.IsKenEnd()) {
			Application.LoadLevel("kencho");
		} else {
			Init();
		}
	}
	void Init() {
		mQuestion.text = QuizQuestion.get ();
		Ken.text = QuizQuestion.getKenName ();
		AnswerMaru.SetActive(false);
		AnswerBatsu.SetActive(false);
		Before.SetActive(true);
		Explanation.gameObject.SetActive(false);
		mState = QuizMainState.LookQuestion;
		btnNext.SetActive(false);
	}
}
