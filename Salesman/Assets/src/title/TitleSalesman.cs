﻿using UnityEngine;
using System.Collections;

public class TitleSalesman : MonoBehaviour {
	public GameObject Body;
	public GameObject Arm;
	public GameObject Head;
	public GameObject Back;
	Vector3 mArmDefault;
	Vector3 mBodyDefault;
	int mTimeRotate;
    bool mRotateRight;
    int mTime;
	// Use this for initialization
	void Start () {
		mTime = 0;
		if (SoundManager.Instance != null)
			SoundManager.Instance.SetVolume (0);
        mTimeRotate = 0;
        mRotateRight = true;
		mArmDefault = Arm.transform.position;
		mBodyDefault = Body.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Back.transform.Rotate (new Vector3 (0, 0, 1));
		int height = mTime - 10;
		height = height  < 0 ? 0 :height;
//		Arm.transform.position = new Vector3 (mArmDefault.x, mArmDefault.y + height, mArmDefault.z);
//		Body.transform.position = new Vector3 (mBodyDefault.x, mBodyDefault.y + height, mBodyDefault.z);
		int bai = 4;
        if (mTimeRotate < 0) {
            if (mRotateRight) {
                Arm.transform.Rotate (new Vector3 (0, 0, bai));
            } else {
                Arm.transform.Rotate(- new Vector3(0, 0, bai));
            }
            mRotateRight = !mRotateRight;
            mTimeRotate = 12;
        }
		mTime--;
        mTimeRotate--;
	}
	public void OnTap() {
		mTime = 30;
	}
}
