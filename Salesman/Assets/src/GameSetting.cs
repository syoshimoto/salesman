﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Ken {
	Tokyo,
	Chiba,
	Kanagawa,
	Ibaraki,
	Saitama,
	Gunma,
	Tochigi,
	Count,
}
public enum QuizType {
	Ken,
	KenDetail,
}

public class GameSetting {
	static Ken mKen = Ken.Chiba;
	static int mCount = 0;
	static int mScoreStudy = 0;
	static int mScoreRoute = 20;
	static List<Ken> mList;
	public static void Init() {
		mList = new List<Ken>();
		for (int i = 0; i < (int)Ken.Count-1; i++) {
//			mList.Add((Ken)i);
		}
		mCount = 0;
		mScoreStudy = 0;
	}
	public static bool ShouldInit() { return mList == null;}
	public static void SetKen(Ken ken) {
		mKen = ken;
		mList.Add (ken);
	}
	public static List<Ken> getList() { return mList;}
	public static void AddCount() {
				mCount++;
		}
	public static void AddStudy(QuizType type) {
		switch (type) {
		case QuizType.Ken:
			mScoreStudy += 2;
			break;
		case QuizType.KenDetail:
			mScoreStudy += 2;
			break;
		}
	}
	public static void ResetCount(){
		mCount = 0;
	}
	public static Ken GetKen() { return mKen;}
	public static int GetCount() { return mCount;}
	public static string GetStudyScore() { return mScoreStudy.ToString();}
	public static bool IsKenEnd() { return mCount == 5;}
}
